# coding: utf-8
BOOK_LIST = [
  'メタプログラミングRuby',
  'これだけ！KPT',
  'プログラミング言語Ruby',
  'How to Get People to Do Staff',
  'JavaScriptで学ぶ関数型プログラミング',
  'リファクタリング/ウェットウェア',
  'アジャイルサムライ',
  'アプレンティスシップ・パターン'
].freeze

namespace :db do
  desc 'Fill database with dummy data'
  task populate_dummy: :environment do
    User.create!(name:  'Example User',
                 email: 'example@railstutorial.org',
                 password: 'foobar',
                 password_confirmation: 'foobar',
                 admin: true)
    99.times do |n|
      name     = Faker::Name.name
      email    = "example-#{n+1}@railstutorial.org"
      password = 'password'
      User.create!(name: name,
                   email: email,
                   password: password,
                   password_confirmation: password)
    end
  end

  desc 'Fill database with test users data'
  task populate_test: :environment do
    group = Group.create!(name: 'Test Group')
    admin = group.users.create!(name:  'Admin',
                                email: 'toku345@gmail.com',
                                password: 'foobar',
                                password_confirmation: 'foobar',
                                admin: true)

    member = group.users.create!(name:  'Test Member',
                                 email: 'test@sample.com',
                                 password: 'foobar',
                                 password_confirmation: 'foobar',
                                 admin: false)

    BOOK_LIST.each do |book_title|
      admin.books.create!(title:  book_title, group_id: admin.group_id)
      member.books.create!(title: book_title, group_id: member.group_id)
    end
  end
end
