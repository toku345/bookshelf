class AddGroupIdUsers < ActiveRecord::Migration
  def change
    add_column :users, :group_id, :integer, default: nil
  end
end
