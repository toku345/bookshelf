require 'spec_helper'

describe Book do

  let(:group) { FactoryGirl.create(:group) }
  let(:user) do
    group.users.create(name: 'Test User', email: 'test@exapmle.com',
                       password: 'foobar', password_confirmation: 'foobar',
                       group_id: group.id)
  end
  let(:book) do
    user.books.build(title: 'The Test Book', group_id: user.group_id,
                     user_id: user.id)
  end

  subject { book }

  it { should respond_to(:title) }
  it { should respond_to(:user_id) }
  it { should respond_to(:group_id) }

  it { should be_valid }

  describe 'when title is no present' do
    before { book.title = ' ' }
    it { should_not be_valid }
  end

  describe 'when group_id is no present' do
    before { book.group_id = ' ' }
    it { should_not be_valid }
  end
end
