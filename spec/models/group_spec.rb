require 'spec_helper'

describe Group do

  let(:group) { FactoryGirl.create(:group) }
  before do
    @user = group.users.build(name: 'Test User', email: 'test@exapmle.com',
                              password: 'foobar', password_confirmation: 'foobar',
                              group_id: group.id)
  end

  subject { group }

  it { should respond_to(:name) }
  it { should respond_to(:users) }
  it { should respond_to(:books) }

  it { should be_valid }

  describe 'when name is no present' do
    before { group.name = ' ' }
    it { should_not be_valid }
  end
end
