require 'spec_helper'

describe 'BookPages' do

  subject { page }

  describe 'new' do
    let(:group) { Group.create(name: 'Test Users') }
    let(:user) do
      group.users.create(name: 'Test User', email: 'test@sample.com',
                         password: 'foobar', password_confirmation: 'foobar',
                         admin: false)
    end
    let(:submit) { 'Add a book' }

    before do
      sign_in user
      visit new_book_path
    end

    it { should have_title('Add a book') }
    it { should have_selector('h1', text: 'Add a book') }

    describe 'with invalid information' do
      before { click_button 'Add a book' }

      it { should have_content 'error' }
      it 'should not create a book' do
        expect { click_button submit }.not_to change(Book, :count)
      end
    end

    describe 'with valid information' do
      before do
        fill_in 'Title', with: 'New Book'
        # click_button 'Add a book'
      end

      it 'should create a book' do
        expect do
          click_button submit
        end.to change(Book.where(user_id: user.id), :count).by(1)
      end

      describe 'view valid page' do
        before { click_button submit }
        it { should have_selector('h3', text: 'My Books') }
      end
    end
  end
end
