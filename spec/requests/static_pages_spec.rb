require 'spec_helper'

describe "Static Pages" do

  subject { page }

  describe "Home page" do
    before { visit root_path }

    it { should have_content('BookShelf') }
    it { should have_title(full_title('')) }
    it { expect(page).not_to have_title('| Home') }
  end

  it "should have the right links on the layout" do
    # root -> sign up
    visit root_path
    click_link "Sign up now!"
    expect(page).to have_title(full_title('Sign up'))

    # sign up -> root
    click_link "Home"
    expect(page).to have_title(full_title(''))
  end
end
