class Book < ActiveRecord::Base
  belongs_to :user
  belongs_to :group
  validates :group_id, presence: true
  validates :title, presence: true
end
