class Group < ActiveRecord::Base
  has_many :users
  has_many :books
  validates :name, presence: true
end
