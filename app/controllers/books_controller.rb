class BooksController < ApplicationController
  def index
    @books = Book.where(group_id: current_user.group_id)
  end

  def new
    @book = current_user.group.books.build
  end

  def create
    @book = Book.new(book_params)
    # @book.group_id = current_user.group_id
    @book.update_attributes(
      group_id: current_user.group_id,
      user_id:  current_user.id
    )

    if @book.save
      flash[:success] = 'A new book added.'
      redirect_to user_books_path(current_user)
    else
      render 'new'
    end
  end

  private

  def book_params
    params.require(:book).permit(:title)
  end
end
